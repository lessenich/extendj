/*
 * ExtendJ is covered by the modified BSD License. You should have received
 * a copy of the modified BSD license with this compiler.
 *
 * Copyright (c) 2011, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 */

aspect SuppressWarnings {
  syn boolean VariableDeclaration.hasAnnotationSuppressWarnings(String annot) =
      getModifiers().hasAnnotationSuppressWarnings(annot);

  inh boolean VariableDeclaration.withinSuppressWarnings(String annot);
  inh boolean FieldDeclaration.withinSuppressWarnings(String annot);
  inh boolean AssignSimpleExpr.withinSuppressWarnings(String annot);
  inh boolean CastExpr.withinSuppressWarnings(String annot);
  inh boolean MethodDecl.withinSuppressWarnings(String annot);
  inh boolean MethodAccess.withinSuppressWarnings(String annot);

  syn boolean VariableDeclaration.suppressWarnings(String type) =
      hasAnnotationSuppressWarnings(type) || withinSuppressWarnings(type);
  syn boolean FieldDeclaration.suppressWarnings(String type) =
      hasAnnotationSuppressWarnings(type) || withinSuppressWarnings(type);
  syn boolean MethodDecl.suppressWarnings(String type) =
      hasAnnotationSuppressWarnings(type) || withinSuppressWarnings(type);
}
